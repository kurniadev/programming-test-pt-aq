class Book {
  final String bookCode;
  final String isbn;
  final String title;
  final String description;
  final String category;
  final String publishDate;
  final String price;
  final bool hasHardCover;

  Book({
    required this.bookCode,
    required this.isbn,
    required this.title,
    required this.description,
    required this.category,
    required this.publishDate,
    required this.price,
    required this.hasHardCover,
  });

  factory Book.fromJson(Map<String, dynamic> json) {
    return Book(
      bookCode: json['bookCode'],
      isbn: json['isbn'],
      title: json['title'],
      description: json['description'],
      category: json['category'],
      publishDate: json['publishDate'],
      price: json['price'],
      hasHardCover: json['hasHardCover'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'bookCode': bookCode,
      'isbn': isbn,
      'title': title,
      'description': description,
      'category': category,
      'publishDate': publishDate,
      'price': price,
      'hasHardCover': hasHardCover,
    };
  }
}
