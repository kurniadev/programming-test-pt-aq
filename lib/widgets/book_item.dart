import 'package:flutter/material.dart';

import '../models/book.dart';

class BookItem extends StatefulWidget {
  const BookItem({super.key, required this.book, required this.delete, required this.update, required this.detail});

  final VoidCallback delete, update, detail;

  final Book book;

  @override
  State<BookItem> createState() => _BookItemState();
}

class _BookItemState extends State<BookItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.detail,
      child: ListTile(
        title: Text(widget.book.title),
        subtitle: Text(widget.book.category),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: widget.update
            ),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: widget.delete
            ),
          ],
        ),
      ),
    );
  }
}
