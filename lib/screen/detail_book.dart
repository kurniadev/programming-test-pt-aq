import 'package:crud_app/screen/detail.dart';
import 'package:crud_app/screen/profile.dart';
import 'package:flutter/material.dart';

import '../models/book.dart';

class DetailBook extends StatefulWidget {
  const DetailBook({super.key, required this.book});

  final Book book;

  @override
  State<DetailBook> createState() => _DetailBookState();
}

class _DetailBookState extends State<DetailBook> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              "Detail Buku",
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
            ),
            bottom: const TabBar(
              indicatorColor: Colors.blue,
              labelColor: Colors.black87,
              labelStyle: TextStyle(
                fontSize: 14,
                color: Colors.black87,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
              tabs: [
                Tab(
                  text: 'Profil',
                ),
                Tab(
                  text: 'Detail',
                ),
              ],
            ),
            backgroundColor: Colors.white,
            iconTheme: const IconThemeData(
              color: Colors.black87,
            ),
            elevation: 0,
          ),
          body: TabBarView(
            children: [
              ProfileScreen(
                book: widget.book,
              ),
              DetailScreen(
                book: widget.book,
              )
            ],
          ),
        ),
      ),
    );
  }
}
