import 'package:flutter/material.dart';

import '../models/book.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({super.key, required this.book});

  final Book book;

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Kode Buku : ${widget.book.bookCode}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              // color: blueColor,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            'Harga Buku : ${widget.book.price}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              // color: blueColor,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            'Kategori Buku : ${widget.book.category}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              // color: blueColor,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            'Tanggal Terbit : ${widget.book.publishDate}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              // color: blueColor,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            'ISBN : ${widget.book.isbn}',
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              // color: blueColor,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            children: [
              Text(
                widget.book.hasHardCover == true
                    ? 'Hard Cover : Iya'
                    : 'Hard Cover : Tidak',
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: blueColor,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}
