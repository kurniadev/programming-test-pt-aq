import 'package:crud_app/models/book.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key, required this.book});

  final Book book;

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 200,
          width: 200,
          child: Image.asset('assets/images/book.png'),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          widget.book.title,
          style: const TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            // color: blueColor,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          widget.book.description,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            // color: blueColor,
          ),
        ),
      ],
    );
  }
}
