import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              "Laporan Keseluruhan",
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
            ),
            bottom: const TabBar(
              indicatorColor: Colors.blueAccent,
              labelColor: Colors.black87,
              labelStyle: TextStyle(
                fontSize: 14,
                color: Colors.black,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
              tabs: [
                Tab(
                  text: 'Harian',
                ),
                Tab(
                  text: 'Bulanan',
                ),
              ],
            ),
            backgroundColor: Colors.white,
            iconTheme: const IconThemeData(
              color: Colors.black,
            ),
            elevation: 0,
          ),
          body: const TabBarView(
            children: [
              // DailyReport(),
              // MonthlyReport(),
              // AnnualyReport(),
            ],
          ),
        ),
      ),
    );
  }
}
