import 'dart:convert';

import 'package:crud_app/screen/add_book.dart';
import 'package:crud_app/screen/dashboard.dart';
import 'package:crud_app/screen/detail_book.dart';
import 'package:crud_app/screen/update_book.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../models/book.dart';
import '../widgets/book_item.dart';

class BookListScreen extends StatefulWidget {
  const BookListScreen({super.key});

  @override
  State<BookListScreen> createState() => _BookListScreenState();
}

class _BookListScreenState extends State<BookListScreen> {
  final storage = const FlutterSecureStorage();
  final List<Book> listBook = [];

  List<Book> filteredBook = [];

  TextEditingController searchController = TextEditingController();
  String searchText = '';

  getDataFromStorage() async {
    String? data = await storage.read(key: 'books');
    if (data != null) {
      final dataDecoded = jsonDecode(data);
      if (dataDecoded is List) {
        setState(() {
          listBook.clear();
          for (var item in dataDecoded) {
            listBook.add(Book.fromJson(item));
          }
          filteredBook = listBook;
          print(filteredBook);
        });
      }
    }
    print(json.encode(listBook));
  }

  void _deleteBook(int index) {
    setState(() {
      listBook.removeAt(index);
    });
    // Simpan perubahan ke penyimpanan
    _saveDataToStorage();
    Navigator.pop(context);
  }

  updateBook(int index, Book updatedBook) {
    setState(() {
      listBook[index] = updatedBook;
    });
    _saveDataToStorage();
  }

  void _searchBooks(String searchText) {
    setState(() {
      filteredBook = listBook.where((book) {
        final titleLower = book.title.toLowerCase();
        final categoryLower = book.category.toLowerCase();
        final searchLower = searchText.toLowerCase();
        return titleLower.contains(searchLower) ||
            categoryLower.contains(searchLower);
      }).toList();
    });
  }

  void _saveDataToStorage() async {
    final jsonData = jsonEncode(listBook);
    await storage.write(key: 'books', value: jsonData);
  }

  @override
  void initState() {
    getDataFromStorage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Book Collection CC : Reza Kurnia'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: searchController,
              decoration: InputDecoration(
                labelText: 'Search',
                prefixIcon: Icon(Icons.search),
              ),
              onChanged: (value) {
                setState(() {
                  searchText = value;
                  print(value);
                  _searchBooks(searchText);
                });
              },
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: filteredBook.length,
              itemBuilder: (ctx, index) {
                return BookItem(
                  book: filteredBook[index],
                  delete: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Error'),
                          content: Text('Please fill in all the fields.'),
                          actions: [
                            TextButton(
                              child: Text('Batal'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            TextButton(
                              child: Text('Oke'),
                              onPressed: () {
                                _deleteBook(index);
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  detail: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailBook(
                                  book: filteredBook[index],
                                )));
                  },
                  update: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UpdateBook(
                                  onUpdate: (updatedBook) {
                                    updateBook(index, updatedBook);
                                  },
                                  book: filteredBook[index],
                                )));
                  },
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          String refresh = await Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddBookScreen()));
          if (refresh == 'refresh') {
            setState(() {
              getDataFromStorage();
            });
          }
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
